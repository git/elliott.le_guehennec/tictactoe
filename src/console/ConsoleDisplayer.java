package console;

import model.Board;
import model.Displayer;
import model.Player;

public class ConsoleDisplayer implements Displayer {
    @Override
    public void display(Board board) {
        for (int row = 1; row <= 3; row++){
            for (int col = 1; col <= 3; col++){
                Player box = board.getBox(col, row);
                char c = box == null ? '-' : box == Player.X ? 'X' : 'O';
                System.out.print(c + " ");
            }
            System.out.println();
        }
    }

    @Override
    public void displayEndMessage(Player winner) {
        if (winner == null) System.out.println("Egalité!");
        else System.out.println((winner == Player.X ? "X" : "O") + " a gagné!");
    }
}
