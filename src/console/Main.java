package console;

import model.Board;
import model.Displayer;
import model.IntPrompter;
import model.Player;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Displayer disp = new ConsoleDisplayer();
        Board b = new Board();
        b.setBox(1, 1, Player.X);
        b.setBox(1, 2, Player.O);
        b.setBox(2, 2, Player.X);
        b.setBox(1, 3, Player.O);
        b.setBox(3, 3, Player.X);
        disp.display(b);
        disp.displayEndMessage(Player.X);
    }
}
