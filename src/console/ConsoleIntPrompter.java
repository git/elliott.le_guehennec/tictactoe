package console;

import model.IntPrompter;

import java.util.Scanner;

public class ConsoleIntPrompter implements IntPrompter {
        private final Scanner sc = new Scanner(System.in);
        @Override
        public int prompt() {
        System.out.println("Entrez un nombre");
        while (!sc.hasNextInt()) sc.next();
        return sc.nextInt();
    }
}
