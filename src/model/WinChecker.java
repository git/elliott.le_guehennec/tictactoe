package model;

public class WinChecker {
    public boolean isGameOver(Board b) {
        if (b.getBox(1, 1) != null) {
            if (b.getBox(1, 1) == b.getBox(1, 2)
                    && b.getBox(1, 2) == b.getBox(1, 3))
                return true;
            if (b.getBox(1, 1) == b.getBox(2, 1)
            && b.getBox(1, 1) == b.getBox(3, 1))
                return true;
            if (b.getBox(1, 1) == b.getBox(2, 2)
                    && b.getBox(1, 1) == b.getBox(3, 3))
                return true;
        }
        if (b.getBox(3, 1) != null) {
            if (b.getBox(3, 1) == b.getBox(3, 2)
                    && b.getBox(3, 1) == b.getBox(3, 3))
                return true;
            if (b.getBox(3, 1) == b.getBox(2, 2)
                    && b.getBox(3, 1) == b.getBox(1, 3))
                return true;
        }
        if (b.getBox(2, 1) != null
            && b.getBox(2, 1) == b.getBox(2, 2)
            && b.getBox(2, 1) == b.getBox(2, 3))
            return true;
        if (b.getBox(1, 2) != null
                && b.getBox(1, 2) == b.getBox(2, 2)
                && b.getBox(1, 2) == b.getBox(3, 2))
            return true;
        if (b.getBox(1, 3) != null
                && b.getBox(1, 3) == b.getBox(2, 3)
                && b.getBox(1, 3) == b.getBox(3, 3))
            return true;
        for (int i = 1; i <= 3; ++i)
            for (int j = 1; j <= 3; ++j)
                if (b.getBox(i, j) == null)
                    return false;
        return true;
    }

    public Player getWinner(Board b) throws IllegalGameStateException {
        Player currentBox, winner = null;
        currentBox = b.getBox(1, 1);
        if (currentBox != null) {
            if (currentBox == b.getBox(1, 2)
                    && currentBox == b.getBox(1, 3))
                winner = currentBox;
            if (currentBox == b.getBox(2, 1)
                    && currentBox == b.getBox(3, 1))
                winner = currentBox;
            if (currentBox == b.getBox(2, 2)
                    && currentBox == b.getBox(3, 3))
                winner = currentBox;
        }
        currentBox = b.getBox(3, 1);
        if (currentBox != null) {
            if (currentBox == b.getBox(3, 2)
                    && currentBox == b.getBox(3, 3)) {
                if (winner != null && winner != currentBox) throw new IllegalGameStateException();
                winner = currentBox;
            }            if (currentBox == b.getBox(2, 2)
                    && currentBox == b.getBox(1, 3)) {
                if (winner != null && winner != currentBox) throw new IllegalGameStateException();
                winner = currentBox;
            }        }
        currentBox = b.getBox(2, 1);
        if (currentBox != null
                && currentBox == b.getBox(2, 2)
                && currentBox == b.getBox(2, 3)) {
            if (winner != null && winner != currentBox) throw new IllegalGameStateException();
            winner = currentBox;
        }        currentBox = b.getBox(1, 2);
        if (currentBox != null
                && currentBox == b.getBox(2, 2)
                && currentBox == b.getBox(3, 2)) {
            if (winner != null && winner != currentBox) throw new IllegalGameStateException();
            winner = currentBox;
        }        currentBox = b.getBox(1, 3);
        if (currentBox != null
                && currentBox == b.getBox(2, 3)
                && currentBox == b.getBox(3, 3)) {
            if (winner != null && winner != currentBox) throw new IllegalGameStateException();
            winner = currentBox;
        }        return winner;
    }
}
