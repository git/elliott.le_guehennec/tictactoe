package model;

public interface Displayer {
    void display(Board board);
    void displayEndMessage(Player winner);
}
