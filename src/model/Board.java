package model;

public class Board {
    Player[] boxes = new Player[9];
    public void setBox(int column, int row, Player value) {
        int index = getIndex(column, row);
        boxes[index] = value;
    }

    public Player getBox(int column, int row) {
        int index = getIndex(column, row);
        return boxes[index];
    }

    private int getIndex(int column, int row) {
        if (row < 1 || row > 3) throw new IndexOutOfBoundsException();
        return (column - 1) * 3 + row - 1;
    }
}
