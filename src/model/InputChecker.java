package model;

public class InputChecker {
    public boolean isInputValid(Board board, int box) {
        if (box < 1 || box > 9) return false;
        int col = (box - 1) / 3 + 1;
        int row = box % 3;
        if (row == 0) row = 3;
        return board.getBox(col, row) == null;
    }
}
