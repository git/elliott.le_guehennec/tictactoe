package model;

public class IllegalGameStateException extends Exception{
    public IllegalGameStateException(){
        super("This gamestate is illegal");
    }
}
