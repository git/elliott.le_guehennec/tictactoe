import model.Board;
import model.Player;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.Random;
import java.util.stream.Stream;

public class BoardTests {
    public static Stream<Arguments> getSetBoxTestData() {
        Random rng = RandomSingleton.get;
        int col = rng.nextInt();
        int row = rng.nextInt();
        boolean shouldThrow = col < 1 || col > 3 || row < 1 || row > 3;
        return Stream.of(
                Arguments.of(1, 1, false),
                Arguments.of(1, 3, false),
                Arguments.of(0, 2, true),
                Arguments.of(4, 2, true),
                Arguments.of(2, 0, true),
                Arguments.of(2, 4, true),
                Arguments.of(col, row, shouldThrow)
        );
    }

    @Test
    public void constructorInitialisesValuesToNull(){
        Board b = new Board();
        for (int i = 1; i <= 3; ++i)
            for (int j = 1; j <= 3; ++j)
                Assertions.assertNull(b.getBox(i, j));
    }

    @ParameterizedTest
    @MethodSource("getSetBoxTestData")
    public void getSetBoxTests(int col, int row, boolean shouldThrow){
        Board b = new Board();
        Player p = Player.X;

        if (shouldThrow){
            Assertions.assertThrows(IndexOutOfBoundsException.class, ()-> b.setBox(col, row, p));
            Assertions.assertThrows(IndexOutOfBoundsException.class, ()-> b.getBox(col, row));
        }
        else {
            b.setBox(col, row, p);
            Assertions.assertEquals(p, b.getBox(col, row));
        }
    }
}
