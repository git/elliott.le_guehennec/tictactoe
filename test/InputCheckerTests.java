import model.Board;
import model.InputChecker;
import model.Player;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

public class InputCheckerTests {
    public static Stream<Arguments> validIndexTestData() {
        int val = RandomSingleton.get.nextInt();
        boolean isValid = val >= 1 && val <= 9;
        return Stream.of(
                Arguments.of(1, true),
                Arguments.of(9, true),
                Arguments.of(-3, false),
                Arguments.of(15, false),
                Arguments.of(val, isValid)
        );
    }

    public static Stream<Arguments> boxOccupiedTestData() {
        Board b = randomBoard();
        int index = RandomSingleton.get.nextInt(9) + 1;
        boolean isValid = true;

        for (int i = 1; i <= 3; ++i)
            for (int j = 1; j <= 3; ++j)
                if (b.getBox(i, j) != null)
                    isValid = false;

        return Stream.of(
                Arguments.of(new Board(), 1,  true),
                Arguments.of(fullBoard(), 1, false),
                Arguments.of(topLeftBoard(), 1, false),
                Arguments.of(topLeftBoard(), 3, true),
                Arguments.of(b, index, isValid
                ));
    }

    private static Board fullBoard(){
        Board b = new Board();
        for (int i = 1; i <= 3; ++i)
            for (int j = 1; j <= 3; ++j)
                b.setBox(i, j, Player.X);
        return b;
    }

    private static Board topLeftBoard(){
        Board b = new Board();
        b.setBox(1, 1, Player.X);
        return b;
    }

    private static Board randomBoard(){
        Board b = new Board();
        for (int i = 1; i <= 3; ++i)
            for (int j = 1; j <= 3; ++j) {
                int rand = RandomSingleton.get.nextInt(3);
                Player p = switch (rand){
                    case 1 -> Player.X;
                    case 2 -> Player.O;
                    default -> null;
                };
                b.setBox(i, j, p);
            }
        return b;
    }

    @ParameterizedTest
    @MethodSource("validIndexTestData")
    public void validIndexTest(int val,boolean expected){
       Board b = new Board();
       boolean actual = new InputChecker().isInputValid(b, val);
       Assertions.assertEquals(expected, actual);
    }

    @ParameterizedTest
    @MethodSource("boxOccupiedTestData")
    public void boxOccupiedTest(Board b, int input, boolean expected){
        boolean actual = new InputChecker().isInputValid(b, input);
        Assertions.assertEquals(expected, actual);
    }
}
